.PHONY: html

html:
	pandoc -f markdown -o README.html README.md -c misc/github-pandoc.css
