# Travail pratique 2

Dans ce deuxième travail pratique, vous devez apporter des modifications à un
programme qui permet de simuler des [automates
cellulaires](https://fr.wikipedia.org/wiki/Automate_cellulaire),

Le travail doit être réalisé **seul**. Il doit être remis au plus tard le **15
juillet 2018** à **23h59**.  À partir de minuit, une pénalité de **2% par
heure** de retard sera appliquée.

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- Se **familiariser** avec un logiciel développé en C par quelqu'un d'autre;
- Ajouter des **nouvelles fonctionnalités** à ce logiciel;
- Documenter convenablement les modifications apportées, en utilisant notamment
  les **requêtes d'intégration**;
- Utiliser correctement un **logiciel de contrôle de version** pour apporter
  les modifications en question.
- S'assurer que les modifications apportées sont adéquates en proposant un
  **cadre de tests** qui illustre le fonctionnement.

## Description du travail

Rendez-vous sur
[https://gitlab.com/ablondin/inf3135-ete2018-tp2](https://gitlab.com/ablondin/inf3135-ete2018-tp2),
où se trouve le simulateur d'automates cellulaires. Vous pouvez consulter son
contenu directement dans un navigateur et vous familiarisez avec celui-ci.

**Note**: cette étape est importante et vous devez prévoir un certain temps
pour vous familiariser avec le programme avant même d'écrire une seule ligne de
code!

Votre travail consiste à compléter les trois tâches ci-bas. Notez qu'elles sont
toutes indépendantes les unes des autres. Bien que ce ne soit pas obligatoire,
il est recommandé de les implémenter dans l'ordre présenté.

### Tâche 1: Mettre les touches en gras dans la légende

Lorsqu'on lance le programme en mode interactif, la légende suivante est
affichée sous la fenêtre qui montre la simulation:

```
┌─ Keys ────────────────────── Step: 1 ──────┐
│*F*: Play forward     *B*: Play backward    │
│*s*: Go to start      *e*: Go to end        │
│*f*: One step forward *b*: One step backward│
│*p*: Pause            *q*: Quit             │
└────────────────────────────────────────────┘
```

Votre première tâche consistera à mieux aérer la légende et remplacer les
expressions de la forme `*<c>*`, où `<c>` est un caractère pour qu'elles
apparaissent plutôt en gras.

Autrement dit, on s'attend à ce que la légende soit plutôt
```
┌─ Keys ───────────────────── Step: 1 ──────┐
│ F: Play forward      B: Play backward     │
│ s: Go to start       e: Go to end         │
│ f: One step forward  b: One step backward │
│ p: Pause             q: Quit              │
└───────────────────────────────────────────┘
```
où les caractères `F`, `s`, `f`, `p`, etc. apparaissent en gras, ce qui est
peut être fait à l'aide de l'attribut `A_BOLD` de la bibliothèque Ncurses.

Dans la requête d'intégration, vous devez inclure une capture d'écran du
comportement de votre programme avant et après modifications.

### Tâche 2: Ajouter de la couleur aux cellules

Lorsqu'on utilise le mode interactif, les cellules actuelles n'apparaissent
qu'en blanc sur fond noir (ou en noir sur fond blanc, selon vos préférences du
terminal).

Dans la deuxième tâche, il vous est demandé d'ajouter des couleurs prédéfinies
aux cellules selon le type de simulation choisie. Vous pouvez choisir les
couleurs que vous voulez en autant que deux cellules de valeurs différentes
soient toujours de couleurs différentes.

Afin de bien documenter votre requête d'intégration, vous devrez inclure des
captures d'écran du comportement de votre programme avant et après
modifications.

### Tâche 3: Lire l'état initial sur l'entrée standard

Ajoutez une option longue `--stdin` qui permet de lire l'état initial de
l'automate sur l'entrée standard. Lorsque l'option `--stdin` est passée, il
n'est pas permis de spécifier les options `-r|--num-rows` et `-c|--num_cols`,
puisque les dimensions peuvent être déduites de l'état initial. Vous devez
également vous assurer que tous les caractères utilisés sont bien valides.

Par exemple, supposez qu'un fichier `etat.txt` contienne les lignes
suivantes:
```
X.HXX
..H..
XX..X
```
Alors on s'attend aux comportement suivants:

1) La commande
```
bin/tp2 -t pandemy -a .XH --stdin < etat.txt
```
produit une simulation valide de 3 lignes et 5 colonnes.

2) La commande
```
bin/tp2 -r 3 -t pandemy -a .XH --stdin < etat.txt
```
affiche le message
```
Error: The number of rows and columns cannot be indicated together with the option --stdin
```

3) La commande
```
bin/tp2 -t pandemy -a .xH --stdin < etat.txt
```
affiche le message
```
Error: The cell state 'X' is not allowed
```
puisque les états permis sont `'.'`, `'x'` et `'H'`.

En revanche, si l'état n'est pas un rectangle, il faut l'indiquer. Ainsi, si le
fichier `etat.txt` contient plutôt ceci
```
X.X
..H..
XX.X
```
alors le message suivant devrait être affiché:
```
Error: All rows and columns should be of the same length
```

N'oubliez pas d'ajouter des tests qui montrent que l'option ajoutée est bel et
bien fonctionnelle.

**Aide**: Par défaut, Ncurses lit sur l'entrée standard (`stdin`) pour se
comporter de façon interactive. Or, si vous lisez l'état initial sur l'entrée
standard, il ne vous sera plus possible de saisir les entrées clavier. Pour
régler ce problème, il suffit d'ajouter la ligne
```c
freopen("/dev/tty", "rw", stdin);
```
juste avant l'appel à la fonction `initscr` qui intialise Ncurses. Voir [cette
discussion](https://stackoverflow.com/questions/8537362/ncurses-app-in-c-reading-standard-input)
sur StackOverflow pour plus d'informations.

### Indépendance des tâches

Il est important de bien diviser les branches et les *commits* selon les tâches
auxquelles ils se rapportent. Notez cependant que dans ce travail pratique, il
n'y a pas de dépendances entre les tâches.

Par conséquent, il est important de bien structurer la "topologie" de vos
contributions pour qu'elle réflète cette indépendance entre les branches. Vos
branches devraient donc être toutes basées sur `master`.

## Soumettre une contribution

Un des objectifs de ce travail pratique est de vous habituer à soumettre vos
modifications en utilisant les mécanismes offerts par le logiciel de contrôle
de versions Git et les plateformes de développement collaboratif telles que
GitLab. Plus précisément, vous devrez utiliser une *requête d'intégration* (en
anglais, *merge request*, aussi parfois appelée *pull request*). Pour cela,
vous devrez créer une branche pour chacune des trois requêtes d'intégration,
que vous devrez nommer respectivement `task1`, `task2` et `task3`. Ces noms
sont importants pour nous permettre de corriger vos travaux de façon
automatique et vous serez pénalisés si vous ne les respectez pas (c'est
extrêmement long à corriger, alors nous devons être rigides sur ce point).

Dans un premier temps, vous devez reproduire (en anglais *fork*) le programme
de base disponible pour qu'il apparaisse dans vos projets sur GitLab.

**Attention!** Assurez-vous de bien faire une copie **privée** (sinon les
autres étudiants pourront voir votre solution et vous serez alors responsable
s'il y a plagiat).

Ensuite, vous devrez donner accès à votre projet en mode **Developer** (pas
**Master**) à l'utilisateur `ablondin`.

Maintenant, supposons que vous êtes satisfait de votre première tâche, qui se
trouve sur la branche `task1`. Alors il vous suffit de pousser la branche sur
votre dépôt, puis de créer une requête d'intégration de la branche `task1` à la
branche `master` de votre dépôt. Il est recommandé de vous inspirer de la
requête d'intégration
[task0](https://gitlab.com/ablondin/inf3135-aut2017-tp2/merge_requests/1) qui a
été fusionnée au projet.

**Attention!** Dans votre travail, vous ne devez **pas** fusionner de branche
lors de la remise de votre travail, car vous devez attendre que vos
modifiations soient acceptées par quelqu'un d'autre, en l'occurrence, moi-même.

**Attention!** En pratique, on fait une requête d'intégration d'une branche de
notre dépôt personnel vers le dépôt principal. Cependant, dans ce contexte
académique, la requête doit être faite à l'intérieur de votre dépôt privé, pour
vous assurer que personne d'autre ne puisse consulter votre code. Ainsi, toutes
vos requêtes se feront d'une branche nommée `task1`, `task2` ou `task3` vers la
branche `master` de votre propre dépôt.

## Tests automatiques

Actuellement, lorsqu'on entre la commande
```
make test
```
une suite de tests est lancée. Lorsque vous apporterez des modifications au
programme, vous devez vous assurer que celles-ci ne brisent pas les suites de
tests (autrement dit, il s'agit de **tests de régression** et vous devez en
tenir compte dans vos modifications).

Lorsque vous allez apporter des modifications au logiciel, vous devrez
également ajouter ou modifier des suites de tests afin d'illustrer que ce que
vous avez fait est fonctionnel et ne brise pas l'état actuel du projet.

Notez qu'il est aussi possible que vous n'ayez aucune modification à apporter,
principalement lorsque vos modifications ne peuvent pas être testées
automatiquement, comme dans le cas d'une modification uniquement graphique. Par
contre, dans ce dernier cas, il est important de joindre une ou plusieurs
captures d'écran, dans la requête d'intégration, qui témoignent de la
contribution.

## Vidéos explicatifs

Lorsqu'on apporte des modifications à un programme, il n'est pas toujours
facile de manipuler les branches et de les partager correctement. Même si vous
êtes expérimentés avec le logiciel Git, je vous recommande fortement de
visualiser au moins une fois les capsules suivantes. Il s'agit d'explications
données à la session d'automne 2016 pour le même cours, mais elles vous seront
utiles dans le cadre de ce travail également:

- [Configurer un environnement de
  travail](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=1fd0c248-6ade-4d27-be27-a83ad4bc7126)
  (les fichiers de configurations mentionnés dans la vidéo sont disponibles
  dans le dossier `exemples` de l'[énoncé des exercices de
  laboratoire](https://gitlab.com/ablondin/inf3135-exercices/tree/master/exemples))
- [Se synchroniser avec le dépôt
  source](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=a65f75af-e3c9-4376-a571-b7d8818f3c91)
- [Modifier sa requête
  d'intégration](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8fcbc94e-5951-42a8-9bda-d7464ac25155)

## Contraintes

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes:

- Votre projet doit être un clone (*fork*) **privé** du projet
  https://gitlab.com/ablondin/inf3135-ete2018-tp2. L'adjectif **privé** est
  très important si vous ne voulez pas que d'autres étudiants accèdent à votre
  solution!
- Vos trois tâches doivent se trouver sur des branches nommées **exactement**
  `task1`, `task2`, `task3`;
- **Aucune requête d'intégration** ne doit être faite vers le dépôt public
  (sans quoi les autres étudiants pourront voir vos modifications!);
- **Aucune requête d'intégration** ne doit être fusionnée sur la branche
  `master` (ou tout autre branche), car elle doit d'abord être validée par
  quelqu'un d'autre (moi-même) avant d'être intégrée, étape qui ne sera pas
  couverte dans le présent travail pratique.
- **Aucune variable globale** (à l'exception des constantes) n'est permise;
- Votre programme doit **compiler** sans **erreur** et sans **avertissement**
  lorsqu'on entre ``make``.

Advenant le non-respect d'une ou plusieurs de ces contraintes, une pénalité de
**50%** sera appliquée systématiquement. Aucune excuse ne sera considérée.

## Remise

Votre travail doit être remis au plus tard le **15 juillet 2018** à **23h59**.
À partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux seront corrigés sur Mac OS et sur le serveur Java. Vous devez donc
vous assurer que votre programme fonctionne **sans modification** sur au moins
une de ces deux plateformes.

## Barème

Pour chacune des trois tâches, les critères suivants seront pris en compte dans
l'évaluation :

**Qualité des "docstrings"**

- Les *docstrings* respectent le standard Javadoc;
- La documentation est bien formatée et bien aérée;
- Le format est cohérent avec les autres *docstrings* déjà présentes dans le
  projet;
- La *docstring* ne contient pas d'erreurs d'orthographe.

**Qualité de la requête d'intégration (*merge request*)**

- Le titre de la requête est significatif;
- La description de la modification apportée est claire, concise et précise. En
  particulier, elle respecte le format Markdown et l'exploite à son maximum.
- Le comportement du programme avant modification est décrit;
- Le comportement du programme après modification est décrit;
- Les messages de *commits* sont significatifs et ont un format cohérent avec
  les messages rédigés par les autres développeurs;
- La requête ne contient pas d'erreurs d'orthographe.

**Qualité du changement de code**

- Le code modifié est lisible, clair et concis;
- Le code est bien aéré, de façon cohérente avec le style de programmation déjà
  existant;
- Il utilise au maximum les fonctionnalités déjà présentes (ne pas réinventer
  la roue);
- La solution n'est pas inutilement complexe.

**Tests automatisés**

- Les modifications ne brisent pas les tests déjà présents;
- S'il y a lieu, des tests sont ajoutés pour illustrer le fait que les
  nouvelles fonctionnalités ont été correctement implémentées.

Plus précisément, j'utiliserai la grille de correction suivante:

| Tâche                   | Points    |
| ----------------------- | --------- |
| Tâche 1                 |    45     |
| Tâche 2                 |    45     |
| Tâche 3                 |    60     |
| **Total**               | **150**   |

Notez que, dans chaque cas, **au moins** la moitié des points concerneront la
qualité de la contribution (requête d'intégration bien rédigée, messages de
*commit* pertinents, atomiques et bien formatés, respect du style, etc.). Il
est donc important non seulement d'apporter une modification correcte du code,
mais aussi de la faire proprement.
